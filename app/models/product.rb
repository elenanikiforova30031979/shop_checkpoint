class Product < ApplicationRecord
  belongs_to :category

  has_one_attached :picture
  validates :picture,
    file_content_type: {
      # mime-тип
      allow: ['image/jpeg', 'image/gif', 'image/png']
    }


  validates :title, presence: true,
            length: { maximum: 250 }

  validates :price, presence: true,
            numericality: { greater_than_or_equal_to: 0 }

end
