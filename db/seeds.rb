# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def copy_image_fixture(product, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{file}.jpg")

  product.picture.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

categories = {
  cpu: Category.create!(name: 'CPUs'),
  ram: Category.create!(name: 'RAM'),
  mb: Category.create!(name: 'Motherboards'),
  hdd: Category.create!(name: 'HDDs')
}


cpu = Product.create!(title: 'Intel Core i7', price: 300, category: categories[:cpu])
copy_image_fixture(cpu, :cpu)

ram = Product.create!(title: '4GB DDR3 RAM', price: 40, category: categories[:ram])
copy_image_fixture(ram, :ram)

hdd = Product.create!(title: '1TB Seagate HDD', price: 60, category: categories[:hdd])
copy_image_fixture(hdd, :hdd)

mb = Product.create!(title: 'Asus P5Q3', price: 120, category: categories[:mb])
copy_image_fixture(mb, :mb)
